<?php
return [
    /**
     * Controller messages
     */
    'days_left_by_company'   => ':key Tariff in company :company ends  Date :date',
    'find_model'             => ':model find successfully',
    'create_model'           => ':model create',
    'update_model'           => ':model update',
    'destroy_model'          => ':model destroy',
    'pay'                    => 'Your payment has been received',
    'pay_invoice'            => 'Your payment, by invoice :invoice , received',
    'find_all_tag'           => 'Tags, by model :model, found',
    'user_register'          => 'User register successfully',
    'logout'                 => 'User logout success',
    'password_reset'         => 'We have e-mailed your password reset link!',
    'password_change'        => 'Password change success!',
    'img_update'             => 'Img in :model update',
    'find_notification'      => 'Notification by company find',



    /**
     * Error messages
     */

    'not_find_model'            => 'Not find model { :model } by id { :id }',
    'model_exist'               => ':model with the same :param , already exist',
    'model_not_exist'           => ':model not exist',
    'device_exist_by_code'      => 'Device with such `code_device` :code_device already exists',
    'model_not_has_property'    => 'Has not property `:property` in model :model ',
    'device_type_not_exist'     => 'Device Type , by code { :type_code } , not exist',
    'hub_exist_by_code'         => 'Hub with such `code_hub` :code_hub already exists',
    'user_email_exist'          => 'User with such email :email already exists',
    'unauthorised'              => 'Unauthorised',
    'not_find_user_by_email'    => 'We can not find a user with that e-mail :email',
    'param_field_not_exist'     => 'param field not exist',
    'param_condition_not_exist' => 'param condition not exist',
    'param_not_exist'           => 'param field not exist',
    'not_have_file'             => 'Not have file',
    'unknown'                   => 'Unknown :param',
    'model_not_exist_param'     => 'Model { :model } with the same :param_name { :param } , not exist',

    /**
     * Info message
     */

    'email_not_verified'  => 'User :user_name ( id :user_id) did not confirm mail!',
    'reset_token_invalid' => 'This password reset token is invalid',
    'token_lifetime'      => 'token lifetime expired',
    'max_limit_tariff'    => 'You have max :param in your tariff plan',
    'not_have_tariff'     => 'You not have tariff plan',

    /**
     * Permissions
     */

    'not_have_permissions_read_in_model' => 'You do not have permission to view the model :model',
    'not_have_permissions_write_in_model' => 'You do not have permission to write the model :model'
];
