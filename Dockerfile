FROM php:7.4-fpm

# Install dependencies

RUN apt-get update && apt-get install -y \
    git \
    curl \
    libpng-dev \
    libonig-dev \
    libxml2-dev \
    zip \
    unzip \
    libzip-dev \
    nano

RUN echo "max_file_uploads=100" >> /usr/local/etc/php/conf.d/docker-php-ext-max_file_uploads.ini
RUN echo "post_max_size=120M" >> /usr/local/etc/php/conf.d/docker-php-ext-post_max_size.ini
RUN echo "upload_max_filesize=120M" >> /usr/local/etc/php/conf.d/docker-php-ext-upload_max_filesize.ini

# Clear cache

RUN apt-get clean && rm -rf /var/lib/apt/lists/*

# Install PHP extensions

RUN docker-php-ext-install pdo_mysql mbstring exif pcntl bcmath gd intl zip
RUN pecl install redis && docker-php-ext-enable redis

# Install composer

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

RUN mkdir /var/backend
COPY . /var/backend/
RUN rm -rf /var/backend/vendor
RUN rm /var/backend/.env
RUN rm -rf /var/backend/tests
RUN rm -rf /var/backend/storage/app/public/*
RUN rm /var/backend/.env.example
RUN rm /var/backend/composer.lock

WORKDIR /var/backend

RUN ln -f -s .env.dev .env
RUN composer install
RUN ln -f -s .env.dev .env
RUN php artisan key:generate
RUN php artisan migrate
RUN php artisan telescope:publish
RUN php artisan telescope:prune --hours=0
RUN php artisan nova:publish --force
RUN php artisan storage:link

CMD php artisan serve --host=0.0.0.0 --port=8000
EXPOSE 8000
