<?php


namespace App\Services;


use App\Models\RequestLog;
use App\Models\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Auth;

class LogService
{
    /**
     * @var
     * Модель , для которой пишется лог
     */
    private $model;

    /**
     * @param $result
     * @param $model
     * Основной цыкл записи лога
     */
    public function log($result, $model){
        $this->model = $model;
        try {
            $caller_info = $this->getCallerInfo();

            if (
                $caller_info["action"] == 'store()' ||
                $caller_info["action"] == 'update()' ||
                $caller_info["action"] == 'destroy()' ||
                $caller_info["action"] == 'delete()' ||
                $caller_info["action"] == 'updateMass()'
            ){
                $log_config = config('Api.log_config');
                $check_need_log = array_search($caller_info["class"],$log_config );

                if ($check_need_log === false){
                    $user = User::find(Auth::id());
                    $log_model = new RequestLog();
                    $log_model->date = Carbon::now()->format('Y-m-d H:i:s');
                    $log_model->user_id = $user->id;
                    $log_model->user_name = $user->name;
                    $log_model->user_email = $user->email;
                    $log_model->company_id = $user->company->id;
                    $log_model->company_name = $user->company->name;
                    $log_model->model = $this->model;
                    $log_model->controller = $caller_info["class"];
                    $log_model->action = $caller_info["action"];
                    $log_model->result = json_encode($result);
                    $log_model->save();

                }
            }
        } catch (Exception $e) {
//            var_dump($e);
        }
    }
    /**
     * Метод для получения родителя и метода который вызван был , для логирования
     * @return array
     */
    public function getCallerInfo() {
        $c = '';
        $func = '';
        $class = '';
        $trace = debug_backtrace();
        if (isset($trace[2])) {
            $func = $trace[2]['function'];
            if ((substr($func, 0, 7) == 'include') || (substr($func, 0, 7) == 'require')) {
                $func = '';
            }
        } else if (isset($trace[1])) {
            $func = '';
        }
        if (isset($trace[3]['class'])) {
            $class = $trace[3]['class'];
            $func = $trace[3]['function'];
        } else if (isset($trace[2]['class'])) {
            $class = $trace[2]['class'];
            $func = $trace[2]['function'];
        }
        $bbb = ($class != '') ?  $class  : "";
        $c = ($func != '') ? $func ."()" : "";

        $result = [
            "class" => $bbb,
            "action" => $c
        ];

        return $result;
    }
}
