<?php

namespace App\Services;

use App\Imports\Elastix\PhoneImportsElastix;

class ParsingPhoneFromExel
{
    protected array $array_phone;
    protected int $company_id;


    public function parsing($request,$company_id)
    {
        $this->company_id = $company_id;
        $path = $this->saveFile($request);
        if (!$path) {
            return false;
        }
        switch ($request->file->extension()) {
            case 'xlsx':
                $this->parsingExel($path);
                break;
        }
        $this->clean_phone();
        $this->addCompanyId();

        return $this->array_phone;
    }

    protected function addCompanyId()
    {
        $result = [];
        foreach ($this->array_phone as $phone_array) {
            $phone = $phone_array["phone"];
            $name = $phone_array["name"];
            array_push($result,["id_campaign"=>$this->company_id,"phone"=>$phone,"firstName"=>$name]);
        }
        $this->array_phone = $result;
    }

    protected function parsingExel($path)
    {
        $result = [];
        $result_array = (new PhoneImportsElastix())->toArray($path);

        foreach ($result_array[0] as $row) {
            if (!isset($row[1])){
                $row[1] = null;
            }
            array_push($result,["phone"=>$row[0],"name"=>$row[1]]);
        }
        $this->array_phone = $result;
    }

    protected function clean_phone()
    {
        $result = [];
        foreach ($this->array_phone as $phone_array) {
            $phone = $phone_array["phone"];
            $name = $phone_array["name"];
            $phone_clean = preg_replace('~\D+~','', $phone);
            $phone_clean = intval($phone_clean);
            if ($phone_clean != 0){
                $check_dublicate = array_search($phone_clean,$result);
                if ($check_dublicate === false) {
                    array_push($result,["phone"=>$phone_clean,"name"=>$name]);
                }
            }
        }
        $this->array_phone = $result;
    }

    protected function saveFile($request)
    {
        $input = $request->file;
        if ($input) {
            $path = $request->file->store('file/csv',env('FILESYSTEM_DRIVER'));
            $path = '../storage/app/public/'.$path;
        }else{
            return false;
        }

        return $path;
    }
}
