<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Api\BaseController;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class LoginController extends BaseController
{
    public $model = User::class;

    public function index(Request $request): JsonResponse
    {
        $input = $request->only(["email","password"]);
        $rule = [
            'email' => 'required|email',
            'password' => 'required',
        ];

        $validator = Validator::make($input,$rule);
        if( $validator->fails() ) {
            return $this->sendError('Find error',$validator->errors());
        }

        $user = User::where('email', $input["email"])->where('status',1)->first();
        if (! $user || ! Hash::check($request->password, $user->password)) {
            $this->sendError('login','error login',1);
        }

        $token = $user->createToken('api-token')->plainTextToken;

        return $this->sendResponse($token,'userToken');
    }
}
