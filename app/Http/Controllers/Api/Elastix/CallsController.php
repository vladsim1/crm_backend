<?php

namespace App\Http\Controllers\Api\Elastix;

use App\Http\Controllers\Api\BaseController;
use App\Models\Elastix\Calls;
use App\Repositories\Elastix\CallsRepository;
use Illuminate\Http\JsonResponse;

class CallsController extends BaseController
{
    public string $model = Calls::class;
    protected $repo;

    /**
     * CallsController constructor.
     */
    function __construct()
    {
        $this->repo = new CallsRepository();
    }

    /**
     * @return JsonResponse
     */
    public function getCountLeftCallsByCompany(): JsonResponse
    {
        $calls = $this->repo->leftCallsByCompany();

        return $this->sendResponse($calls,'Find!');
    }
}
