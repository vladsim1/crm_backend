<?php

namespace App\Http\Controllers\Api\Elastix;

use App\Http\Controllers\Api\BaseController;
use App\Models\Elastix\Calls;
use App\Repositories\Elastix\AddPhoneFromFileRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class AddPhoneFromFileController extends BaseController
{
    public string $model = Calls::class;
    protected $repo;

    /**
     * CallsController constructor.
     */
    function __construct()
    {
        $this->repo = new AddPhoneFromFileRepository();
    }

    public function addPhoneToBaseInFile(Request $request): JsonResponse
    {
        $result = $this->repo->addPhoneToCallsFromFile($request);

        return $this->sendResponse($result,'Add!');
    }
}
