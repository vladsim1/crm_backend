<?php

namespace App\Http\Controllers\Api;

use App\Models\Office;
use App\Repositories\OfficeRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class OfficeController extends BaseController
{
    public string $model = Office::class;
    protected $repo;

    function __construct()
    {
        $this->repo = new OfficeRepository($this->model);
    }

    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $offices = $this->repo->allByCompany();

        return $this->sendResponse($offices, "all offices");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {

        $data = $this->repo->validate($request);
        $office = $this->repo->checkAndCreate($data);

        return $this->sendResponse($office,'office create');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        $office = $this->repo->findByCompany($id);

        return $this->sendResponse($office, 'office find');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(Request $request, int $id): JsonResponse
    {
        $this->repo->rule([
            "title" => "max:255",
            "description" => "max:8000"
        ]);
        $data = $this->repo->validate($request);
        $office = $this->repo->checkAndUpdate($id,$data);

        return $this->sendResponseObj($office,'office Update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {
        $office = $this->repo->checkAndDestroy($id);

        return $this->sendResponse($office,'office Destroy');
    }
}
