<?php

namespace App\Http\Controllers\Api;

use App\Services\LogService;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller as Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;


class BaseController extends Controller
{
    /**
     * @var array Хедеры для отправки ответа
     */
    public array $headers = [
        'Access-Control-Allow-Origin' => '*',
        'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS',
        'Access-Control-Allow-Headers' => 'Authorization, Content-Type',
        'Access-Control-Allow-Credentials' => 'True',
    ];

    /**
     * success response method.
     *
     * @param $result
     * @param $message
     * @return JsonResponse
     */
    public function sendResponse($result, $message): JsonResponse
    {
        $response = [
            'success' => true,
            'data'    => $result,
            'message' => $message,
        ];

        $log_service = new LogService();
        $log_service->log($response,$this->model);

        return response()->json($response,200, [], JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT)->withHeaders($this->headers);
    }

    /**
     * Метод для отправки ответа обэктом
     * @param $result
     * @param $message
     * @return JsonResponse
     */
    public function sendResponseObj($result, $message): JsonResponse
    {
        $response = [
            'success' => true,
            'data'    => $result,
            'message' => $message,
        ];

        $log_service = new LogService();
        $log_service->log($response,$this->model);

        return response()->json($response,200, [], JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT)->withHeaders($this->headers);
    }

    /**
     * @param $result
     * @param $message
     * @return JsonResponse
     */
    public function sendResponseNoLog($result, $message): JsonResponse
    {
        $response = [
            'success' => true,
            'data'    => $result,
            'message' => $message,
        ];

        return response()->json($response,200, [], JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT)->withHeaders($this->headers);
    }

    /**
     * return error response.
     *
     * @param $error
     * @param array $errorMessages
     * @param null $repo
     * @param int $code
     * @return JsonResponse
     */
    public function sendError($error, $errorMessages = [], $repo = null, $code = 200): JsonResponse
    {
        $response = [
            'success' => false,
            'message' => $errorMessages,
        ];

        if( !empty($errorMessages) ) {
            $response['data'] = [];
        }
        if ( $repo != null) {
            abort( response()->json($response, $code)->withHeaders($this->headers) );
        }
        return response()->json($response, $code)->withHeaders($this->headers);
    }

    /**
     * Метод для отправки модели юзера
     * @param $result
     * @return ResponseFactory|Response
     */
    public function sendResponseUser($result)
    {
        $res = collect($result);

        $result = $res->map( function ($item, $key) {
            foreach ($item as $k => $v) {
                if (is_array($v)) {
                    foreach ($v as $ke => $ve) {
                        if (is_null($ve)) {
                            $item[$k][$ke] = '';
                        }
                    }
                } else {
                    if (is_null($v)) {
                        $item[$k] = '';
                    }
                }
            }
            return $item;
        });

        if ( !isset( $result[0] ) ) {
            $response['user'] = [];
        }

        $response['user'] = $result[0];
        $response['success'] = true;

        return response($response)->withHeaders($this->headers);
    }
}
