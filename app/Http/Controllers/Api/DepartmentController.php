<?php

namespace App\Http\Controllers\Api;

use App\Models\Department;
use App\Repositories\DepartmentRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class DepartmentController extends BaseController
{
    public string $model = Department::class;
    protected $repo;

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */

    function __construct()
    {
        $this->repo = new DepartmentRepository($this->model);
    }

    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $departments = $this->repo->allByCompany();

        return $this->sendResponse($departments, "all departments");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {

        $data = $this->repo->validate($request);
        $department = $this->repo->checkAndCreate($data);

        return $this->sendResponse($department,'department create');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        $department = $this->repo->findByCompany($id);

        return $this->sendResponse($department, 'department find');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(Request $request, int $id): JsonResponse
    {
        $this->repo->rule(["title" => "max:255","description" => "max:8000"
        ]);
        $data = $this->repo->validate($request);
        $department = $this->repo->checkAndUpdate($id,$data);

        return $this->sendResponseObj($department,'department Update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {
        $department = $this->repo->checkAndDestroy($id);

        return $this->sendResponse($department,'department Destroy');
    }
}
