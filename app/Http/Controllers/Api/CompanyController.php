<?php

namespace App\Http\Controllers\Api;

use App\Models\Company;
use App\Repositories\CompanyRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CompanyController extends BaseController
{
    public string $model = Company::class;
    protected $repo;

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */

    function __construct()
    {
        $this->repo = new CompanyRepository($this->model);
    }

    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(): Response
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request): Response
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show(int $id): Response
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit(int $id): Response
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function update(Request $request): JsonResponse
    {
        $this->repo->rule([
            "email" => "email:rfc,dns"
        ]);
        $data = $this->repo->validate($request,["name","website","email","phone","address"]);
        $user = $this->repo->checkAndUpdate($data);

        return $this->sendResponseObj($user,'User Update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy(int $id): Response
    {
        //
    }
}
