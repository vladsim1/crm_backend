<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Lead;
use App\Repositories\LeadRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class LeadController extends BaseController
{
    public string $model = Lead::class;
    protected $repo;

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */

    function __construct()
    {
        $this->repo = new LeadRepository($this->model);
    }

    public function index(): JsonResponse
    {
        $lead = $this->repo->allByCompany();
        foreach ($lead as $lead_v){
            $lead_v = $this->repo->userDetails($lead_v);
        }

        return $this->sendResponse($lead, "all departments");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request): JsonResponse
    {

        $data = $this->repo->validate($request);
        $lead = $this->repo->checkAndCreate($data);

        return $this->sendResponse($lead,'lead create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(int $id): JsonResponse
    {
        $lead = $this->repo->findByCompany($id);
        $lead = $this->repo->userDetails($lead);

        return $this->sendResponse($lead, 'lead find');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $id): JsonResponse
    {
        $this->repo->rule(["description" => "max:8000"
        ]);
        $data = $this->repo->validate($request);
        $lead = $this->repo->checkAndUpdate($id,$data);

        return $this->sendResponseObj($lead,'lead Update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id): JsonResponse
    {
        $lead = $this->repo->checkAndDestroy($id);

        return $this->sendResponse($lead,'lead Destroy');
    }
}
