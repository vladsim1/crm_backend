<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\Base;
use App\Models\User;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends BaseController
{
    public string $model = User::class;
    protected $repo;

    /**
     * Display a listing of the resource.
     *
     * @return bool
     */

    function __construct()
    {
        $this->repo = new UserRepository($this->model);
        return true;
    }

    public function current()
    {
        $user = $this->repo->findGet(Auth::id());

        return $this->sendResponseUser(new Base($user));
    }

    public function index(): \Illuminate\Http\JsonResponse
    {
        $users = $this->repo->allByCompany();

        return $this->sendResponse($users, "all users");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request): \Illuminate\Http\JsonResponse
    {
        $data = $this->repo->validate($request);
        $user = $this->repo->checkAndCreate($data);

        return $this->sendResponse($user,'User Create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id): \Illuminate\Http\JsonResponse
    {
        $user = $this->repo->findByCompany($id);

        return $this->sendResponse($user, 'User Find');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id): \Illuminate\Http\JsonResponse
    {
        $this->repo->rule([
            "email" => "email:rfc,dns"
        ]);
        $data = $this->repo->validate($request);
        $user = $this->repo->checkAndUpdate($id,$data);

        return $this->sendResponseObj($user,'User Update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id): \Illuminate\Http\JsonResponse
    {
        $user = $this->repo->checkAndDestroy($id);

        return $this->sendResponse($user,'User Destroy');
    }
}
