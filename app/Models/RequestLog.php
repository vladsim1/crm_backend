<?php

namespace App\Models;

use DateTimeInterface;
use Illuminate\Database\Eloquent\Model;

class RequestLog extends Model
{
    protected $guard_name = 'api';

    protected $fillable = [
        'date', 'user_id','user_name','user_email','company_id','company_name','model','action','result','controller'
    ];

    /**
     * Prepare a date for array / JSON serialization.
     *
     * @param  \DateTimeInterface  $date
     * @return string
     */
    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    protected $casts = [
        'date' => 'date:Y-m-d H:i:s'
    ];
}
