<?php

namespace App\Models;

use DateTimeInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    use HasFactory;

    protected $fillable = [
        'id', 'title', 'description','company_id'
    ];
    protected $guard_name = 'api';

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function user(){
        return $this->hasMany(User::class);
    }

    public function company(){
        return $this->belongsTo(Company::class);
    }

    public function lead(){
        return $this->hasMany(Lead::class);
    }
}
