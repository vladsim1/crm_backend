<?php

namespace App\Models\Elastix;

use DateTimeInterface;
use Illuminate\Database\Eloquent\Model;

class Campaign extends Model
{
    protected $connection = 'mysql_elastix';
    protected $table = 'campaign';

    protected $fillable = [
        'id', 'name', 'datetime_init','datetime_end','daytime_init','daytime_end',
        'retries','trunk','context','queue','max_canales','num_completadas',
        'promedio','desviacion','script','estatus','id_url',
    ];
    protected $guard_name = 'api';

    /**
     * Prepare a date for array / JSON serialization.
     *
     * @param  \DateTimeInterface  $date
     * @return string
     */
    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
}
