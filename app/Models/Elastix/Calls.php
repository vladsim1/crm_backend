<?php

namespace App\Models\Elastix;

use DateTimeInterface;
use Illuminate\Database\Eloquent\Model;

class Calls extends Model
{
    protected $connection = 'mysql_elastix';
    protected $table = 'calls';

    protected $fillable = [
        'id', 'id_campaign', 'phone','status','uniqueid','fecha_llamada',
        'start_time','end_time','retries','duration','id_agent','transfer',
        'datetime_entry_queue','duration_wait','dnc','date_init','date_end',
        'time_init','time_end','agent','failure_cause','failure_cause_txt',
        'datetime_originate','trunk','scheduled','id_city','leng','id_status',
        'firstName','lastName',
    ];
    protected $guard_name = 'api';

    /**
     * Prepare a date for array / JSON serialization.
     *
     * @param  \DateTimeInterface  $date
     * @return string
     */
    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
}
