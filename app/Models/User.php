<?php

namespace App\Models;

use DateTimeInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens, HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','company_id','department_id','surname',
        'phone','position','email_verification_token','email_verified',
        'email_verified_at','role_id','telegram_chat_id','f_name',
        'login','pass_asterisk','office_id','lang_id','extension','birthday',
        'start_work','dismissal','theme_id','status',
    ];

    protected $guard_name = 'api';

    protected $with = ['company','department','lang','office'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Prepare a date for array / JSON serialization.
     *
     * @param  \DateTimeInterface  $date
     * @return string
     */
    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function company(){
        return $this->belongsTo(Company::class);
    }

    public function department(){
        return $this->belongsTo(Department::class);
    }

    public function lang(){
        return $this->belongsTo(Lang::class);
    }

    public function office(){
        return $this->belongsTo(Office::class);
    }

    public function lead(){
        return $this->hasMany(Lead::class);
    }
}
