<?php

namespace App\Models\Asterisk;

use DateTimeInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cdr extends Model
{
    protected $connection = 'mysql_asterisk';
    protected $table = 'cdr';

    protected $fillable = [
        'calldate', 'clid', 'src','dst','dcontext','channel',
        'dstchannel','lastapp','lastdata','duration','billsec','disposition',
        'amaflags','accountcode','uniqueid','userfield','did',
        'cnum','recordingfile','cnam','outbound_cnum','outbound_cnam',
        'dst_cnam',
    ];
    protected $guard_name = 'api';

    /**
     * Prepare a date for array / JSON serialization.
     *
     * @param  \DateTimeInterface  $date
     * @return string
     */
    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
}
