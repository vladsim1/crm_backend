<?php

namespace App\Models;

use DateTimeInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lead extends Model
{
    use HasFactory;

    protected $fillable = [
        'id', 'name', 'lname', 'fname', 'passport', 'description', 'status', 'user_add_id', 'user_resp_id', 'department_id','company_id'
    ];
    protected $guard_name = 'api';

    protected $with = ['department'];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function department(){
        return $this->belongsTo(Department::class);
    }

    public function user_add_id(){
        return $this->belongsTo(User::class, 'user_add_id');
    }

    public function user_resp_id(){
        return $this->belongsTo(User::class, 'user_resp_id');
    }

    public function company(){
        return $this->belongsTo(Company::class);
    }

}
