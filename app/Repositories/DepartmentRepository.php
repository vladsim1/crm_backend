<?php

namespace App\Repositories;

use App\Helper\ApiHelper;

class DepartmentRepository extends BaseRepository
{
    public function __construct($model)
    {
        $this->model = $model;

        $this->rule();

        $this->initialize_response();
    }

    public function rule($rule = null)
    {
        if ($rule == null) {
            $this->rule = ["title" => "required|max:255","description" => "max:8000"];
        }else{
            $this->rule = $rule;
        }
    }

    public function checkAndCreate($data)
    {
        $data = $this->addToData($data,['company_id' => ApiHelper::getCompanyId()]);

        $department = $this->create($data);

        return $department;
    }

    public function checkAndUpdate($id, $data)
    {
        $department = $this->update($id,$data);

        return $department;
    }

    public function checkAndDestroy($id)
    {
        $department = $this->destroy($id);

        return $department;
    }
}
