<?php

namespace App\Repositories;

use App\Helper\ApiHelper;

class OfficeRepository extends BaseRepository
{
    public function __construct($model)
    {
        $this->model = $model;

        $this->rule();

        $this->initialize_response();
    }

    public function rule($rule = null)
    {
        if ($rule == null) {
            $this->rule = ["title" => "required|max:255","description" => "max:8000"];
        }else{
            $this->rule = $rule;
        }
    }

    public function checkAndCreate($data)
    {
        $data = $this->addToData($data,['company_id' => ApiHelper::getCompanyId()]);

        $office = $this->create($data);

        return $office;
    }

    public function checkAndUpdate($id, $data)
    {
        $office = $this->update($id,$data);

        return $office;
    }

    public function checkAndDestroy($id)
    {
        $office = $this->destroy($id);

        return $office;
    }
}
