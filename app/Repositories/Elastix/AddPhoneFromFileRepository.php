<?php

namespace App\Repositories\Elastix;

use App\Services\ParsingPhoneFromExel;

class AddPhoneFromFileRepository extends BaseElastixRepository
{
    protected array $array_phone;
    protected int $company_id;

    public function addPhoneToCallsFromFile($request)
    {
        $this->company_id = $request->company_id;
        $service = new ParsingPhoneFromExel();
        $this->array_phone = $service->parsing($request,$this->company_id);
        $this->deleteOldPhoneInDatabase();
        $this->insertPhoneToDatabase();
        $this->activateCompany();

        return $this->array_phone;
    }

    protected function insertPhoneToDatabase()
    {
        $this->model_calls::insert($this->array_phone);
    }

    protected function deleteOldPhoneInDatabase()
    {
        $result = $this->model_calls::where('id_campaign', $this->company_id)->where('status', null)->delete();

        return $result;
    }

    protected function activateCompany()
    {
        $company = $this->model_campaign::find($this->company_id);
        $company->estatus = 'A';
        $company->timestamps = false;
        $company->save();

        return $company;
    }
}
