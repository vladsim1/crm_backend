<?php

namespace App\Repositories\Elastix;

use App\Models\Elastix\Calls;
use App\Models\Elastix\Campaign;
use App\Repositories\BaseRepository;

class BaseElastixRepository extends BaseRepository
{
    protected string $model_calls;
    protected string $model_campaign;

    public function __construct()
    {
        $this->initialize_response();
        $this->model_calls = Calls::class;
        $this->model_campaign = Campaign::class;
    }

    protected function getActiveCompaniesIdElastix(): array
    {
        $campaigns = $this->model_campaign::where('estatus','A')->get();
        $result = [];
        foreach ($campaigns as $campaign) {
            array_push($result,$campaign->id);
        }

        return $result;
    }

    protected function getNameCompanyElastix(int $id): string
    {
        return $this->model_campaign::where('id',$id)->value('name');
    }
}
