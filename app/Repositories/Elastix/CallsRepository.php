<?php

namespace App\Repositories\Elastix;


class CallsRepository extends BaseElastixRepository
{
    /**
     * @return array
     * Метод возвращает колличество оставшихся номеров для прозвона по активным задачам
     */
    public function leftCallsByCompany(): array
    {
        $companies_id = $this->getActiveCompaniesIdElastix();
        $result = [];

        foreach ($companies_id as $company_id) {
            $calls_left = $this->model_calls::where('id_campaign',$company_id)->where('status',null)->count();
            $company_name = $this->getNameCompanyElastix($company_id);
            $result[$company_name]=$calls_left;
        }

        return $result;
    }
}
