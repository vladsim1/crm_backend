<?php

namespace App\Repositories;

use App\Helper\ApiHelper;

class CompanyRepository extends BaseRepository
{
    /**
     * UserRepository constructor.
     * @param $model
     */
    public function __construct($model)
    {
        /**
         * Присвоение модели с которой будет работа
         */
        $this->model = $model;
        /**
         * Инициализация правил для валидации
         */
        $this->rule();
        /**
         * Инициализация BaseController для отправки ответов
         */
        $this->initialize_response();
    }

    /**
     * @param null $rule
     * установка правил валидации
     */
    public function rule($rule = null)
    {
        if ($rule == null) {
            $this->rule = ["name" => "required",];
        }else{
            $this->rule = $rule;
        }
    }

    public function checkAndUpdate($data)
    {
        $model_obj = $this->model::find(ApiHelper::getCompanyId());
        $model_obj->fill($data);
        $model_obj->save();


        return $this->model::find(ApiHelper::getCompanyId());
    }
}
