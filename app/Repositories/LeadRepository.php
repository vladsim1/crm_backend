<?php


namespace App\Repositories;


use App\Helper\ApiHelper;
use App\Models\User;

class LeadRepository extends BaseRepository
{
    public function __construct($model)
    {
        $this->model = $model;

        $this->rule();

        $this->initialize_response();
    }

    public function rule($rule = null)
    {
        if ($rule == null) {
            $this->rule = ["status" => "required","user_add_id" => "required","department_id" => "required","description" => "max:8000"];
        }else{
            $this->rule = $rule;
        }
    }

    public function checkAndCreate($data)
    {
        $data = $this->addToData($data,['company_id' => ApiHelper::getCompanyId()]);

        $lead = $this->create($data);

        return $lead;
    }

    public function checkAndUpdate($id, $data)
    {
        $lead = $this->update($id,$data);

        return $lead;
    }

    public function checkAndDestroy($id)
    {
        $lead = $this->destroy($id);

        return $lead;
    }

    public function  userDetails($lead)
    {

        $lead['user_add_id'] = User::select('id', 'name', 'surname', 'position', 'email', 'department_id')->where('id',$lead['user_add_id'])->without('company','department','lang','office')->get();
        $lead['user_resp_id'] = User::select('id', 'name', 'surname', 'position', 'email', 'department_id')->where('id',$lead['user_resp_id'])->without('company','department','lang','office')->get();


        return $lead;
    }
}
