<?php

namespace App\Helper;

use App\Models\Company;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class ApiHelper
{
    /**
     * @return int
     */
    public static function getCompanyId(): int
    {
        return User::find(Auth::id())->company->id;
    }

    /**
     * @return object
     */
    public static function getCompanyUser(): object
    {
        return Company::find(self::getCompanyId());
    }

    /**
     * @return int
     */
    public static function getUserRoleById(): int
    {
        return User::find( Auth::id() )->role_id;
    }

    /**
     * @param $array
     * @param $field
     * @param int $sort
     * @return array
     */
    public static function customMultiSort($array, $field, $sort = SORT_DESC): array
    {
        $sortArr = array();
        foreach($array as $key=>$val){
            $sortArr[$key] = $val[$field];
        }

        array_multisort($sortArr ,$sort,$array);

        return $array;
    }

    /**
     * @return int
     */
    public static function getDepartmentId(): int
    {
        return User::find(Auth::id())->department->id;
    }
}
