<?php

use App\Http\Controllers\Api\DepartmentController;
use App\Http\Controllers\Api\Elastix\AddPhoneFromFileController;
use App\Http\Controllers\Api\Elastix\CallsController;
use App\Http\Controllers\Api\LeadController;
use App\Http\Controllers\Api\OfficeController;
use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Api\CompanyController;
use Illuminate\Support\Facades\Route;

Route::post('login', [LoginController::class,'index']);

Route::middleware('auth:sanctum')->group(function(){
    /**
     * User Routes
     */
    Route::get('users/current',[UserController::class , 'current']);
    Route::resource('users', UserController::class);
    /**
     * Company Routes
     */
    Route::resource('company', CompanyController::class);
    /**
     * Department Routes
     */
    Route::resource('department', DepartmentController::class);
    /**
     * Office Routes
     */
    Route::resource('office', OfficeController::class);
    /**
     * Lead Routes
     */
    Route::resource('lead', LeadController::class);

    /**
     * Elastix Route
     */
    Route::get('elastix/calls_left',[CallsController::class,'getCountLeftCallsByCompany']);
    Route::post('elastix/add_phone_file',[AddPhoneFromFileController::class,'addPhoneToBaseInFile']);

});
